package com.example.realhousehelps;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.*;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realhousehelps.APIs.APIs;
import com.example.realhousehelps.controllers.AppController;
import com.example.realhousehelps.helper.CheckNetworkStatus;
import com.example.realhousehelps.helper.session_manager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class Register extends AppCompatActivity {
    private ProgressDialog pDialog;
    private session_manager session;

    //private SQLiteHandler db;
Button  btnregister;
EditText  txtfname,txtlname,txtemail,txtphone,txtpassword,cntxtpassword,passhint;
TextView logintxtview ;
//TextInputEditText

//EditText txtnames,txtemail,txtphone,txtpassword;
private static final String TAG = Register.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Registe");
        btnregister=findViewById(R.id.btnsign_up);
        txtfname=findViewById(R.id.fname);
        txtlname=findViewById(R.id.lname);
        txtemail=findViewById(R.id.email_address);
        txtphone=findViewById(R.id.phone);
        txtpassword=findViewById(R.id.password);
        cntxtpassword=findViewById(R.id.cnpassword);
        passhint=findViewById(R.id.hint);
        logintxtview=findViewById(R.id.sign_in);

        //validate email
        txtphone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                String myphon= txtphone.getText().toString();
                isValidPhoneNumber(myphon);
            }
        });
        txtemail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                validatemail();
            }
        });


        logintxtview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),login.class));
            }
        });
        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        // Session manager
        session = new session_manager(getApplicationContext());

        // SQLite database handler
        //db = new SQLiteHandler(getApplicationContext());

       //  Check if user is already logged in or not
        //session.checkLogin();
//        finish();
        btnregister.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (CheckNetworkStatus.isNetworkAvailable(getApplicationContext())) {
                    String firstname = txtfname.getText().toString().trim();
                    String lastname = txtlname.getText().toString().trim();
                    String email = txtemail.getText().toString().trim();
                    String phone = txtphone.getText().toString().trim();
                    phone= phone.replaceFirst("^0*","254");
                    String password = txtpassword.getText().toString().trim();
                    String cnfpassword = cntxtpassword.getText().toString().trim();
                    String passwordhint = passhint.getText().toString().trim();

                    if (!firstname.isEmpty() && !lastname.isEmpty() && !email.isEmpty() && !phone.isEmpty() && !password.isEmpty() && !cnfpassword.isEmpty()&& !passwordhint.isEmpty()) {
                        registerUser(firstname,lastname, email, phone, password,cnfpassword,passwordhint);
                        if(!password.equals(cnfpassword)){
                            Toast.makeText(Register.this, "Passwords do not match", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(),
                                "Please enter  your details!", Toast.LENGTH_LONG)
                                .show();
                    }
                }
                else{
                    Toast.makeText(getApplicationContext(),
                            "Unable to connect to Internet", Toast.LENGTH_LONG)
                            .show();
                }

            }
        });

    }
    private void registerUser(final String first_name,final String last_name, final String email,final String phone,
                              final String password,final String confirm_password,final String pass_hint) {
        Log.e(TAG, "registerUser: start");
        // Tag used to cancel the request
        String tag_string_req = "req_register";

        pDialog.setMessage("Registering ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                APIs.Register_Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Register Response: " + response);
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("success");

                    Log.e(TAG, "onResponse: " + error);
                    if (error) {
                        // User successfully stored in MySQL
                        // Now store the user in sqlite
                        Log.e(TAG, "onResponse: success");
                        JSONObject access_token = jObj.getJSONObject("token");
                        String uid = access_token.getString("access_token");
                        JSONObject user = jObj.getJSONObject("user");
                        String myfname = user.getString("first_name");
                        String mylname = user.getString("last_name");
                        String email = user.getString("email");
                        String phone = user.getString("phone");
                        String created_at = user
                                .getString("created_at");

                        Toast.makeText(getApplicationContext(), "User successfully registered!", Toast.LENGTH_LONG).show();
                        // Launch login activity
                        startActivity(new Intent(getApplicationContext(),login.class));
                        finish();
                    }
                    else
                        {
                            Toast.makeText(Register.this, "Make sure you have entered correct details", Toast.LENGTH_LONG).show();

                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Registration Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "An error occured retry" , Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("first_name", first_name);
                params.put("last_name", last_name);
                params.put("email", email);
                params.put("phone", phone);
                params.put("password", password);
                params.put("password_confirmation", confirm_password);
                params.put("password_hint", pass_hint);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    public boolean validatemail(){
        String emailInput = txtemail.getText().toString().trim();
        if(emailInput.isEmpty()){
            txtemail.setError("Email cannot be Empty");
            return false;
        }
        else if(!Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()){
            txtemail.setError("Email invalid");
            return false;
        }
        else {
            txtemail.setError(null);
            return true;
        }
    }
    public static final boolean isValidPhoneNumber(CharSequence target) {
        if (target.length() > 7 &&target.length() <14) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(target).matches();
        }
    }
}

