package com.example.realhousehelps;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.realhousehelps.Retrof.Ret_Api;
import com.example.realhousehelps.Retrof.Retr;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Search_Results extends AppCompatActivity {
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_househelp_listing);

        ///
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ///
        Intent intent=getIntent();
        Bundle extras = intent.getExtras();
        String location = extras.getString("COUNTY");
        int category = extras.getInt("CAT");
        String gender = extras.getString("GENDER");
        String placement = extras.getString("PLACEMENT");
        String name = extras.getString("NAME");
        display_search_results(location,category,gender,placement,name);
        recyclerView = findViewById(R.id.rec);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(false);
    }
    //display all results searched.
    public  void display_search_results(String location,int category,String gender,String placement, String name){
        Ret_Api ret_api= Retr.createService2(Ret_Api.class);
        ret_api.getSearchResults(location,category,gender,placement,name).enqueue(new Callback<List<househelps_model>>() {
            @Override
            public void onResponse(Call<List<househelps_model>> call, Response<List<househelps_model>> response) {
                List<househelps_model> model=response.body();
                for (int i=0; i<model.size();i++) {
                    househelps_adapter adapter = new househelps_adapter(Search_Results.this, model);
                    recyclerView.setAdapter(adapter);               }

            }

            @Override
            public void onFailure(Call<List<househelps_model>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "hellooo"+t, Toast.LENGTH_SHORT).show();


            }
        });

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
