package com.example.realhousehelps;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.*;
import android.os.Bundle;
import android.view.MenuItem;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.realhousehelps.APIs.APIs;
import com.example.realhousehelps.helper.session_manager;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class user_profile extends AppCompatActivity {
session_manager session;
private TextView myemail,myfname,myphone,mylast_login,mylname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        LinearLayout edit_profile = (LinearLayout )findViewById(R.id.profilePostsContainer);
//load user pic and details
        loaduser();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        myemail=(TextView) findViewById(R.id.email_tv);
        myfname=(TextView)findViewById(R.id.fname_tv);
        mylname=(TextView)findViewById(R.id.lname_tv);
        myphone=(TextView)findViewById(R.id.phone_tv);
        mylast_login=(TextView)findViewById(R.id.bio);

        session=new  session_manager(this);
        HashMap<String, String> user = session.getUserDetails();
        String fname = user.get(session_manager.KEY_FNAME);
        String lname = user.get(session_manager.KEY_LNAME);
        String email = user.get(session_manager.KEY_EMAIL);
        String phone = user.get(session_manager.KEY_PHONE);
       // String last_login = user.get(session_manager.KEY_CREATED_AT);
        myemail.setText(email);
        myfname.setText(fname);
        mylname.setText(lname);
        myphone.setText(phone);
        //mylast_login.setText(last_login);

        edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent edit=new Intent(getApplicationContext(),Edit_profile.class);
                edit.putExtra("fname",fname);
                edit.putExtra("lname",lname);
                edit.putExtra("email",email);
                edit.putExtra("phone",phone);
                startActivity(edit);
            }
        });

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    public void loaduser(){
        session=new  session_manager(this);
        HashMap<String, String> user = session.getUserDetails();
        String id = user.get(session_manager.KEY_ID);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, APIs.user_details+id,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //converting the string to json array object
                            JSONObject obj = new JSONObject(response);
                            JSONObject array = obj.getJSONObject("data");
                            ImageView prof_image=findViewById(R.id.prof_pic);
                            TextView bio= findViewById(R.id.bio);
                            bio.setText(array.getString("bio"));

                            String url_image = array.getString("avatar");
                            String URL_users="http://192.168.0.189/users/avatar/";
                            Glide.with(getApplicationContext())
                                    .load(URL_users+url_image)
                                    .thumbnail(0.5f)
                                    .fitCenter()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(prof_image);


                        } catch (JSONException e) {
                            Toast.makeText(user_profile.this, "ERROR" + e, Toast.LENGTH_LONG).show();

                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(" error",error.toString());
                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);



    }


}
