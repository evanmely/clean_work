package com.example.realhousehelps.Payments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.realhousehelps.R;

public class Paymemt_Menus extends AppCompatActivity {
CardView make_payment,payhistory,pendingpay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notifications);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

make_payment=(CardView)findViewById(R.id.make_payment);
payhistory=(CardView)findViewById(R.id.history_payment);
payhistory.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
       startActivity(new Intent(getApplicationContext(),Payment_History.class));
    }
});
make_payment.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        startActivity(new Intent(getApplicationContext(),BankPayment.class));
    }
});
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
