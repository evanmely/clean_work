package com.example.realhousehelps.Payments;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.realhousehelps.R;
import com.example.realhousehelps.Retrof.Ret_Api;
import com.example.realhousehelps.Retrof.Retr;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BankPayment extends AppCompatActivity {
    Button send;
    EditText code;
    String Transa_cod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank__payment);
        code=findViewById(R.id.cod);
        send=findViewById(R.id.button);
        Transa_cod=code.getText().toString();
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Transa_cod.isEmpty()){

                    makepay();
                }
                else {

                }
            }
        });

    }

    public  void makepay(){
        Ret_Api ret_api= Retr.createService(Ret_Api.class);
        Call<String> call=ret_api.bank_payment("","");
       call.enqueue(new Callback<String>() {
           @Override
           public void onResponse(Call<String> call, Response<String> response) {

           }

           @Override
           public void onFailure(Call<String> call, Throwable t) {

           }
       });
    }
}
