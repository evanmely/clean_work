package com.example.realhousehelps.Payments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realhousehelps.APIs.APIs;
import com.example.realhousehelps.R;
import com.example.realhousehelps.Register;
import com.example.realhousehelps.Retrof.Ret_Api;
import com.example.realhousehelps.Retrof.Retr;
import com.example.realhousehelps.controllers.AppController;
import com.example.realhousehelps.helper.session_manager;
import com.example.realhousehelps.login;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class Payment extends AppCompatActivity {
    Button make_payment;
    TextView plan,amount,monthly_plan;
    EditText textView;
    String amt;
    session_manager session;
    int index, amm;
    String Payment_plan;
    String Payment_mode;
    String userid="0725850798";
    Dialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        plan=findViewById(R.id.plan_type);
        monthly_plan=findViewById(R.id.plan_month);
        amount=findViewById(R.id.amount_cost);
        Intent intent=getIntent();
        String amount_int =intent.getStringExtra("amount");
        String plan_int =intent.getStringExtra("plan");
        monthly_plan.setText(amount_int);
           amm=Integer.parseInt(amount_int);
            if(amm==2240){
                amm=amm*3;
            }
            else if (amm==1600){
                amm=amm*6;
            }
            else if (amm==960){
                amm=amm*12;
            }
            amount.setText(""+amm);

        plan.setText(plan_int);
        make_payment=findViewById(R.id.pay);
        RadioGroup radioGroup=findViewById(R.id.radio_payment);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                View radioButton = radioGroup.findViewById(i);
                index = radioGroup.indexOfChild(radioButton);
                switch (index) {
                    case 0: // Mpesa

                        break;
                    case 1: //Bank

                        break;


                }
            }
        });

        make_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (index==0){
                    Payment_mode="Mpesa";
                    showDialog(Payment.this, "Confirmed you want to pay " + amm + " to househelp account");



                }
                else{
                    Payment_mode="Bank";
                    BankDialog(Payment.this);


                }



            }
        });


    }


// mke payment for the housegirl
    private void makepayment() {
        session = new session_manager(this);
        String hlp_id =  getIntent().getStringExtra("househelp_id");
        // Tag used to cancel the request
        String tag_string_req = "req_Pay";

        progress_dialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                APIs.Payment_Url, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String Checkout_request_Id = jObj.getString("CheckoutRequestId");
                    Log.e("boolean_success", "onResponse: " + Checkout_request_Id);
                    if (Checkout_request_Id.equals(1)) {
                        Toast.makeText(getApplicationContext(), "Your Payment has been received Successfully!", Toast.LENGTH_LONG).show();
                        // Launch login activity
//                        startActivity(new Intent(getApplicationContext(), login.class));
                         finish();
                    }
                    else
                    {

                        Toast.makeText(Payment.this, "An error ocurred while making payments", Toast.LENGTH_SHORT).show();

                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Payment", "payment Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Please wait for " +error, Toast.LENGTH_LONG).show();
              progressDialog.hide();
              progressDialog.cancel();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                HashMap<String, String> user = session.getUserDetails();
               int id= 3;
                int hid= 3;
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id",String.valueOf(id));
                params.put("househelp_id", String.valueOf(hid));
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }






    public void showDialog(Activity activity, String msg){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(msg);

        Button cancel = (Button) dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_pay);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //call function to send request to push stk.
                makepayment();
               dialog.dismiss();
                //progress_dialog();

            }
        });

        dialog.show();

    }
    public void BankDialog(Activity activity){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.bank_dialog);

        Button cancel = (Button) dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_pay);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Payment.this, BankPayment.class));
                dialog.dismiss();
                progress_dialog();

            }
        });

        dialog.show();

    }

    public  void progress_dialog (){

         progressDialog = new Dialog(this);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.custom_dialog_progress);
        /* Custom setting to change TextView text,Color and Text Size according to your Preference*/
        TextView progressTv = progressDialog.findViewById(R.id.progress_tv);
        progressTv.setText(getResources().getString(R.string.loading));
        progressTv.setTextColor(ContextCompat.getColor(this, R.color.colorAccent));
        progressTv.setTextSize(19F);
        if(progressDialog.getWindow() != null)
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        progressDialog.setCancelable(false);
        progressDialog.show();


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
