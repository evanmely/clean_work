package com.example.realhousehelps.Payments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.realhousehelps.R;

public class Payment_Plans extends AppCompatActivity {
    String amount="h";
    String plan="h";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.premium_dialog);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        setSingleEvent(findViewById(R.id.grid));
        Button pay=findViewById(R.id.pay);
        String hid =  getIntent().getStringExtra("househelp_id");
        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!plan.equals("h")){
                    Intent intent = new Intent(getApplicationContext(), Payment.class);
                    intent.putExtra("amount", amount);
                    intent.putExtra("plan", plan);
                    intent.putExtra("househelp_id",hid);
                    startActivity(intent);

                }

                else {
                    Toast.makeText(getApplicationContext(),"Select Plan",Toast.LENGTH_LONG).show();


                }

            }
        });
    }



    public void setSingleEvent(LinearLayout maingrid) {
        for (int i = 0; i < maingrid.getChildCount(); i++) {
            final CardView cardView = (CardView) maingrid.getChildAt(i);
            final int finalI = i;
            cardView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {

                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        Toast.makeText(getApplicationContext(), "Selected: " + finalI, Toast.LENGTH_SHORT).show();
                        switch (finalI) {
                             case 0: // setting payment_plan to 3 months
                              //   maingrid.refreshDrawableState();

                                  maingrid.getChildAt(1).setBackgroundResource(R.drawable.rounded_button_drawable);
                                  maingrid.getChildAt(2).setBackgroundResource(R.drawable.rounded_button_drawable);
                                  cardView.setBackgroundResource(R.drawable.rounded_button_drawable_but);

                                 plan="3 Months";
                              amount="1600";

                                 break;
                            case 1: //setting payment_plan to 6 months
                               // maingrid.clearChildFocus(i);
                                maingrid.getChildAt(2).setBackgroundResource(R.drawable.rounded_button_drawable);
                                maingrid.getChildAt(0).setBackgroundResource(R.drawable.rounded_button_drawable);
                                cardView.setBackgroundResource(R.drawable.rounded_button_drawable_but);

                                plan="Annual";
                                amount="960";
                                break;

                            case 2: //setting payment_plan to annual
                              //  maingrid.refreshDrawableState();
                                maingrid.getChildAt(1).setBackgroundResource(R.drawable.rounded_button_drawable);
                                maingrid.getChildAt(0).setBackgroundResource(R.drawable.rounded_button_drawable);
                                cardView.setBackgroundResource(R.drawable.rounded_button_drawable_but);

                                plan="6 Months";
                                amount="2240";
                                break;


                        }
                    } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {

                        /* Reset Color */
                        cardView.setBackgroundResource(R.drawable.rounded_button_drawable_but);


                    }

                    return false;
                }
            });

        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}




