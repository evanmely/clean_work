package com.example.realhousehelps;

public class househelps_model {
    private int id;
    private String first_name;
    private String marital_status;
    private String dob;
    private String gender;
    private String type_of_placement;
    private String imgURL;

    public househelps_model(int id, String first_name, String marital_status, String dob, String gender, String type_of_placement , String imgURL) {

        this.id = id;
        this.first_name = first_name;
        this.marital_status = marital_status;
        this.dob = dob;
        this.gender = gender;
        this.type_of_placement = type_of_placement;
        this.imgURL = imgURL;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMarital_status() {
        return marital_status;
    }

    public void setMarital_status(String marital_status) {
        this.marital_status = marital_status;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getType_of_placement() {
        return type_of_placement;
    }

    public void setType_of_placement(String type) {
        this.type_of_placement = type_of_placement;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }
}
