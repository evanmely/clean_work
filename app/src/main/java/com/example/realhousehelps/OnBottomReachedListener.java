package com.example.realhousehelps;

public interface OnBottomReachedListener {
    void onBottomReached(int position);

}
