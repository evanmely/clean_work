package com.example.realhousehelps.model;

public class Datum {
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
