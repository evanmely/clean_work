package com.example.realhousehelps;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import android.util.*;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realhousehelps.APIs.APIs;
import com.example.realhousehelps.controllers.AppController;
import com.example.realhousehelps.helper.CheckNetworkStatus;
import com.example.realhousehelps.helper.session_manager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class login extends AppCompatActivity {
    session_manager mysession;
    private static final String TAG = Register.class.getSimpleName();
    Button btnlogin;
    TextView SignUp;
    EditText phonetxt,passwordtxt;
    private ProgressDialog pDialog;
    //private SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnlogin=findViewById(R.id.login);
        SignUp=findViewById(R.id.sign_up);
        passwordtxt=findViewById(R.id.password);
        passwordtxt.setText("12345678");
        phonetxt = findViewById(R.id.phoneNumber);
        phonetxt.setText("0725850798");
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
       // db= new SQLiteHandler(this);

        SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),Register.class));
            }
        });

        btnlogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {


                if (CheckNetworkStatus.isNetworkAvailable(getApplicationContext())) {


                    String phone = phonetxt.getText().toString().trim();
                    phone= phone.replaceFirst("^0*","254");
                    String password = passwordtxt.getText().toString().trim();

                    // Check for empty data in the form
                    if (!phone.isEmpty() && !password.isEmpty()) {
                        // login user
//                        startActivity(new Intent(getApplicationContext(),user_landing.class
//                        ));
                        checkLogin(phone, password);
                    } else {
                        // Prom
                        // pt user to enter credentials
                        Toast.makeText(getApplicationContext(),
                                "Please enter the credentials!", Toast.LENGTH_LONG)
                                .show();
                    }
                }
                else {
                    Toast.makeText(login.this,
                            "Unable to connect to internet",
                            Toast.LENGTH_LONG).show();

                }
            }

        });
    }
    private void checkLogin(final String phone, final String password) {
        mysession = new session_manager(this);
        // Tag used to cancel the request
        String tag_string_req = "req_login";
        pDialog.setMessage("Logging in  please wait...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                APIs.Login_Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG,"Login Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean success = jObj.getBoolean("success");

                    // Check for error node in json
                    if (success) {
                        // user successfully logged in
                        // Create login session
                        JSONObject user = jObj.getJSONObject("user");
                        String user_id = user.getString("id");
                        String fname = user.getString("first_name");
                        String lname = user.getString("last_name");
                        String email = user.getString("email");
                        String phone = user.getString("phone");
                        JSONObject access_token = jObj.getJSONObject("token");
                        String api_token = access_token.getString("access_token");
                        String created_at = user.getString("created_at");
                        //Toast.makeText(getApplicationContext()," ACCESS _TOKEN"+api_token,Toast.LENGTH_LONG).show();
                       // creating session
                        mysession.createLoginSession(user_id,fname,lname,email,phone,api_token,created_at);
                        // Launch user_landing
                        Toast.makeText(getApplicationContext()," successfully Logged in",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(login.this,
                                pre_landing.class);
                        startActivity(intent);
                        finish();
                    }
                    else{
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("message");
                        Toast.makeText(getApplicationContext(),"credentials  entered are wrong"
                                +errorMsg, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "There was an error connecting with the server", Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getApplicationContext(),"credentials  entered are wrong"
                        , Toast.LENGTH_LONG).show();
                //Log.e(TAG, "Login Error: " + error.getMessage());
//                Toast.makeText(getApplicationContext(),
//                      error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("phone", phone);
                params.put("password", password);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
