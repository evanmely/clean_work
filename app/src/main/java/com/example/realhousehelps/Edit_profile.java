package com.example.realhousehelps;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.loader.content.CursorLoader;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.util.Base64;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.realhousehelps.APIs.APIs;
import com.example.realhousehelps.controllers.AppController;
import com.example.realhousehelps.helper.session_manager;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.security.auth.login.LoginException;

public class Edit_profile extends AppCompatActivity {
    private TextInputEditText myemail, myphone, myfirstname, mylastname, mybio;
    Button updatebtn;
    ImageView imageView;
    Bitmap bitmap;
    String filePath;
    session_manager session;
    final int REQUEST_CODE_GALLERY = 111;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        myemail = findViewById(R.id.email);
        myfirstname = findViewById(R.id.first_name);
        mylastname = findViewById(R.id.last_name);
        myphone = findViewById(R.id.phone);
        mybio = findViewById(R.id.bio);
        imageView = findViewById(R.id.updateimage);
        updatebtn = findViewById(R.id.btnupdate);
        String fname = getIntent().getStringExtra("fname");
        String lname = getIntent().getStringExtra("lname");
        String email = getIntent().getStringExtra("email");
        String phone = getIntent().getStringExtra("phone");
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        myphone.setText(phone);
        myemail.setText(email);
        myfirstname.setText(fname);
        mylastname.setText(lname);
        session = new session_manager(this);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //read external storage
                ActivityCompat.requestPermissions(
                        Edit_profile.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_CODE_GALLERY
                );
            }
        });
        ///update prof
        updatebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageUpload();
            }
        });
    }



    public void imageUpload() {
        session = new session_manager(this);
        //initializing sessionmanager object
        HashMap<String, String> user = session.getUserDetails();
        String access_token = user.get(session_manager.KEY_SESSION_API);
        String user_id = user.get(session_manager.KEY_ID);
        String firstname = myfirstname.getText().toString().trim();
        String lastname = mylastname.getText().toString().trim();
        String prof_email = myemail.getText().toString().trim();
        String prof_bio = mybio.getText().toString().trim();

        // Tag used to cancel the request
        String tag_string_req = "reg_update";
        Log.e("", "imageUpload: "+imagetoString(bitmap));
        progressDialog.setMessage("updating Your Details ...");
        progressDialog.show();
        //Toast.makeText(this, APIs.Updateprof +"?api_token="+access_token, Toast.LENGTH_LONG).show();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                APIs.Updateprof, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                progressDialog.dismiss();

                try {
                JSONObject object = new JSONObject(response);
                boolean message =object.getBoolean("success");
                if(message==true) {
//
                Toast.makeText(Edit_profile.this, "Update was successful", Toast.LENGTH_LONG).show();

                }
                else{
                    Toast.makeText(Edit_profile.this, "Failed", Toast.LENGTH_LONG).show();
                }
                } catch
                (JSONException e) {
                    Log.e("" + e, "");

                    Toast.makeText(Edit_profile.this, ""+e, Toast.LENGTH_LONG).show();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error", "This is unsuccessful: " + error);
                Toast.makeText(getApplicationContext(), "could not process request" + error, Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url

                Map<String, String> params = new HashMap<String, String>();
                params.put("first_name", firstname);
                params.put("last_name", lastname);
                params.put("bio", prof_bio);
                params.put("avatar", imagetoString(bitmap));
                params.put("email", prof_email);
                params.put("id", user_id);
//                params.put("api_token", access_token);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json");
//                headers.put("api_token",access_token);
                return headers;
            }
        };
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public void onRequestPermissionsResult ( int requestCode, @NonNull String[] permissions,
                                             @NonNull int[] grantResults){
        if (requestCode == REQUEST_CODE_GALLERY) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, REQUEST_CODE_GALLERY);
            } else {
                Toast.makeText(this, "You DON'T HAVE PERMISSION", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
    @Override
    protected void onActivityResult ( int requestCode, int resultCode, @Nullable Intent data){
        if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            try {
                InputStream inputStream = getContentResolver().openInputStream(uri);
                bitmap = BitmapFactory.decodeStream(inputStream);
                imageView.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);

    }
//convert to encodedString
    private String imagetoString (Bitmap bitmap){
        ByteArrayOutputStream outputStream= new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,outputStream);
        byte[] imageBytes= outputStream.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes,Base64.DEFAULT);
        return  "data:image/png;base64," +encodedImage;
    }
    //go back to previous activity
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
