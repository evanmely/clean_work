package com.example.realhousehelps.Retrof;


import android.text.GetChars;

import com.example.realhousehelps.househelps_model;
import com.example.realhousehelps.model.Category_model;
import com.example.realhousehelps.model.Loca_Model;
import com.example.realhousehelps.model.Mod;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Ret_Api {
    @GET("househelps")
    Call<househelps_model> getAllHousehelp();

    @GET("zich")
    Call<List<Mod>> getall();

    @FormUrlEncoded
    @POST("insert")
    Call<String> insert(@Field("name") String phone, @Field("pass")
            String amount);
    @FormUrlEncoded
    @POST("insert")
    Call<String> bank_payment(@Field("phone") String phone, @Field("code")
            String code);

    @GET("counties")
    Call<List<Loca_Model>> getLocation();

    @GET("househelp-categories")
    Call<Category_model> getAllcat();


    @GET("search")
    Call<List<househelps_model>> getSearchResults(@Query("county_id") String location, @Query("category") int category, @Query("gender") String genger, @Query("placementtype") String placement,@Query("name") String name);


}
