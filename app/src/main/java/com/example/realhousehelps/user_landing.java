package com.example.realhousehelps;
//import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.realhousehelps.APIs.APIs;
import com.example.realhousehelps.Payments.Paymemt_Menus;
import com.example.realhousehelps.Payments.Payment_History;
import com.example.realhousehelps.Retrof.Ret_Api;
import com.example.realhousehelps.Retrof.Retr;
import com.example.realhousehelps.helper.session_manager;
import com.example.realhousehelps.model.Category_model;
import com.example.realhousehelps.model.Datum;
import com.example.realhousehelps.model.Loca_Model;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import android.widget.ArrayAdapter;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class user_landing extends AppCompatActivity {
    DrawerLayout mdrawerLayout;
    ActionBarDrawerToggle drawerToggle;
    NavigationView navigation;
    session_manager session;
    Spinner county,category,placement_spin,gender_spin;
    TextView user_text,editname;
    String H_County,H_placement,name;

    String gender;
    int H_category;
    Button Search_button;
    private RequestQueue queue;
    MaterialBetterSpinner spinnerlocation;
    ArrayList<String> listItems=new ArrayList<>();
    ArrayAdapter<String> adapter;
    CardView househelps,payments;
    List<String> list = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_landing);
        session= new session_manager(this);

//load user details
        loaduser();
        //populate counties
        populateCounty();
        //pop categories (Nannies/caregivers etc)
        getCategory();
        county=findViewById(R.id.county);
        category=findViewById(R.id.cat);
        Search_button=findViewById(R.id.search_button);
        placement_spin=(Spinner)findViewById(R.id.pplace);
        gender_spin=findViewById(R.id.gender);
        editname=findViewById(R.id.name);
        String name= editname.getText().toString();

///



        //gender spinner activities

        gender_spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                gender= gender_spin.getSelectedItem().toString();
                //Toast.makeText(getApplicationContext(),"this is gender"+gender,Toast.LENGTH_LONG).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
//placement spinner
        placement_spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                H_placement= placement_spin.getSelectedItem().toString();
                //Toast.makeText(user_landing.this, ""+H_placement, Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //activity of search button
        Search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(),Search_Results.class);
                i.putExtra("COUNTY",H_County);
                i.putExtra("CAT",H_category);
                i.putExtra("GENDER",gender);
                i.putExtra("PLACEMENT",H_placement);
                i.putExtra("NAME",name);

                startActivity(i);

                //Toast.makeText(getApplicationContext(),""+H_category+H_County,Toast.LENGTH_LONG).show();


            }
        });

        session= new session_manager(this);
        user_text=(TextView)findViewById(R.id.user_text);
        HashMap<String, String> user = session.getUserDetails();
        String fname = user.get(session_manager.KEY_FNAME);
        String lname = user.get(session_manager.KEY_LNAME);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setSubtitle(fname  +" " +lname);

        initInstances();
    }

    private void initInstances() {

        mdrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerToggle = new ActionBarDrawerToggle(this, mdrawerLayout, R.string.open, R.string.close);
        mdrawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        (getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        navigation = (NavigationView) findViewById(R.id.navigation_view);
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.profile:
                        startActivity(new Intent(getApplicationContext(),user_profile.class));
                        // add navigation drawer item onclick method here
                        break;
                    case R.id.notifications:

                        break;
                    case R.id.payments:
                        startActivity(new Intent(getApplicationContext(), Payment_History.class));
                        break;
                    case R.id.help:

                        break;


                    case R.id.all_list:
                        startActivity(new Intent(getApplicationContext(),househelp_listing.class));
                        break;


                    case R.id.settings:
                        //Do some thing here
                        // add navigation drawer item onclick method here
                        break;
                    case R.id.logout:
                        session.logoutUser();
                        break;
                }
                return true;
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (drawerToggle.onOptionsItemSelected(item))
            return true;


        return super.onOptionsItemSelected(item);
    }

    //select all categories and populate to a spinner
    public void getCategory(){
        Ret_Api api=Retr.createService2(Ret_Api.class);
        api.getAllcat().enqueue(new Callback<Category_model>() {
            @Override
            public void onResponse(Call<Category_model> call, Response<Category_model> response) {

                try {
                    Category_model category_model = response.body();
                    //Toast.makeText(getApplicationContext(), "" + category_model.getData().get(0).getName(), Toast.LENGTH_LONG).show();



                    List<String> list1 = new ArrayList<String>();

                    for(int i=0;i<category_model.getData().size();i++) {
                        list1.add(category_model.getData().get(i).getName());
                        ArrayAdapter<String> arrayAdapter= new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_item,list1);
                        category.setAdapter(arrayAdapter);
                        category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                H_category= position+1;


                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });





                    }


                    //getting the whole json object from the response
//
//                    JSONObject obj = new JSONObject(response.body());
//                    JSONArray dataArray  = obj.getJSONArray("data");
//
//                   Category_model category_model=new Category_model();
//                    for (int i = 0; i < dataArray.length(); i++) {
//                        Datum datum=new Datum();
//
//                        JSONObject dataobj = dataArray.getJSONObject(i);
//                        datum.setName(dataobj.getString("name"));
//                        Toast.makeText(getApplicationContext(), "" + datum.getName(), Toast.LENGTH_LONG).show();
//
//
//
//
//                    }





                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "" +e, Toast.LENGTH_LONG).show();

                    e.printStackTrace();
                }


            }



            @Override
            public void onFailure(Call<Category_model> call, Throwable t) {

            }
        });

    }


    //getting all location and populating to spinner.
    public void populateCounty(){
        Ret_Api ret_api= Retr.createService2(Ret_Api.class);
        ret_api.getLocation().enqueue(new Callback<List<Loca_Model>>() {
            @Override
            public void onResponse(Call<List<Loca_Model>> call, Response<List<Loca_Model>> response) {
                List<Loca_Model> models=response.body();
                for(int i=0;i<models.size();i++) {
                    list.add(models.get(i).getCounty());
                    ArrayAdapter<String> arrayAdapter= new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_item,list);
                    county.setAdapter(arrayAdapter);
                    county.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            H_County= county.getSelectedItem().toString();


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<List<Loca_Model>> call, Throwable t) {
                Toast.makeText(getApplicationContext(),""+t,Toast.LENGTH_LONG).show();


            }
        });


    }
public void loaduser(){
        session=new session_manager(this);
    HashMap<String, String> user = session.getUserDetails();
    String id = user.get(session_manager.KEY_ID);

    StringRequest stringRequest = new StringRequest(Request.Method.GET, APIs.user_details+id,
            new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {

                        //converting the string to json array object
                        JSONObject obj = new JSONObject(response);
                        JSONObject array = obj.getJSONObject("data");
                        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
                        View headerView = navigationView.getHeaderView(0);
                        TextView navemail = (TextView) headerView.findViewById(R.id.navemail);
                        TextView navfname=(TextView)headerView.findViewById(R.id.navfname);
                        TextView navlname=(TextView)headerView.findViewById(R.id.navlname);
                        ImageView myimage=findViewById(R.id.myimageview);

                        navfname.setText(array.getString("first_name"));
                        navlname.setText(array.getString("last_name"));
                        navemail.setText(array.getString("email"));
                        String url_image = array.getString("avatar");
                        String URL_users="http://192.168.0.189/users/avatar/";
                        Glide.with(getApplicationContext())
                                .load(URL_users+url_image)
                                .thumbnail(0.5f)
                                .fitCenter()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(myimage);


                    } catch (JSONException e) {
                        Toast.makeText(user_landing.this, "ERROR" + e, Toast.LENGTH_LONG).show();

                        e.printStackTrace();
                    }
                }
            },
            new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(" error",error.toString());
                }
            });

    //adding our stringrequest to queue
    Volley.newRequestQueue(this).add(stringRequest);



}


}

